package matthewwilliamson.p05_williamson;

import java.util.Random;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.view.View;

/**
 * Created by matthewwilliamson on 2/25/16.
 */

public class Fruit {
	private Bitmap bitmap;
	private float x;
	private float y;
    private float dx;
    private float dy;
    private int border;
    private float a;
    private float da;
    private Matrix matrix;
    private final int[] fruitImages = {R.drawable.apple, R.drawable.banana, R.drawable.coconut,
            R.drawable.grape, R.drawable.green_apple, R.drawable.green_grape,
            R.drawable.orange, R.drawable.star};

	public Fruit(Context context, View v) {
		Random rand = new Random();
        this.bitmap = BitmapFactory.decodeResource(context.getResources(),
                fruitImages[rand.nextInt(fruitImages.length)]);
        this.border = v.getWidth() - 10;
        this.x = rand.nextInt(border - 20) + 20;
		this.y = v.getHeight() - 20;
        dx = rand.nextInt(20) * (rand.nextInt(2) * 2 - 1);
        dy = -(rand.nextInt(20) + 20);
        matrix = new Matrix();
        a = 0;
        da = rand.nextInt(2) * 2 - 1;
	}

	public float getY() {
		return y;
	}

    public void update() {
        dy += 0.5;
        x += dx;
        if (x < 10 || border < x) {
            dx = -dx/2;
            x += dx;
        }
        y += dy;
        if (y < 5) {
            dy = 5;
            y += dy;
        }
        a += da;
        matrix.setTranslate(x, y);
        matrix.postRotate(a, x - bitmap.getWidth()/2, y - bitmap.getHeight()/2);
    }

	public void draw(Canvas canvas) {
        canvas.drawBitmap(bitmap, matrix, null);
    }

	public boolean hit(float eventX, float eventY) {
		if (eventX >= (x - bitmap.getWidth() / 2) && (eventX <= (x + bitmap.getWidth() / 2)) &&
				eventY >= (y - bitmap.getHeight() / 2) && (y <= (y + bitmap.getHeight() / 2))) {
				return true;
			}
        return false;
	}
}
